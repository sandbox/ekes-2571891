Under Active Development
------------------------
Please be aware that this module is under active development and may be updated
at any time.

Description
-----------
VoIP Webform makes a VoipDrupal dialplan script from a standard Drupal Webform.
It can then send and receive calls with the included incoming call router.

Requirements
------------
Drupal 6.x
VoIP Drupal Package (http://drupal.org/project/voipdrupal)
Webform Module (http://drupal.org/project/webform)
VoIP Service Provider (Tropo, Twilio, Plivo)

Installation
------------
1. Download the required packages (VoIPDrupal, Webform, VoIPWebform).

2. Login as an administrator. Navigate to "Administer" -> "Site Building" ->
   "Modules" and enable the following modules: VoIP Call, VoIP Drupal Core,
   VoIP Webform, Webform, as well as one of the VoIP service provider modules
   (VoIP Tropo, VoIP Twilio, or VoIP Plivo).

3. For more information about options available in configuring the module, see
   the documentation page at http://drupal.org/node/1515112

Upgrading from previous versions
--------------------------------
Not applicable at this moment.

Support
-------
Please use the issue queue for filing bugs with this module at
http://drupal.org/project/issues/voipwebform

